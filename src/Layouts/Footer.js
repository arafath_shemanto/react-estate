import React from "react";

const Footer = () => {
  return (
    <footer className="home_one_footer">
      <div className="main_footer_wrap">
        <div className="container-fluid">
          <div className="row">
            <div className="col-xl-3 col-lg-3 col-md-6">
              <div className="footer_widget">
                <div className="footer_title">
                  <h3>Support</h3>
                </div>
                <ul className="footer_links">
                  <li>
                    <a href="#">Help Center</a>
                  </li>
                  <li>
                    <a href="#">Safety information</a>
                  </li>
                  <li>
                    <a href="#">COVID-19 Response</a>
                  </li>
                  <li>
                    <a href="#">Supporting people with disabilities</a>
                  </li>
                  <li>
                    <a href="#">Contact With Agents</a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-xl-3 col-lg-3 col-md-6">
              <div className="footer_widget">
                <div className="footer_title">
                  <h3>Popular Searches</h3>
                </div>
                <ul className="footer_links">
                  <li>
                    <a href="#">Apertment for Rents Near Me</a>
                  </li>
                  <li>
                    <a href="#">Cheap Apertment Rents Near Me</a>
                  </li>
                  <li>
                    <a href="#">Pet Friendly Apertment Rents Near Me</a>
                  </li>
                  <li>
                    <a href="#">Houses For Rrent Near Me</a>
                  </li>
                  <li>
                    <a href="#">More</a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-xl-3 col-lg-3 col-md-6">
              <div className="footer_widget">
                <div className="footer_title">
                  <h3>For Professionals</h3>
                </div>
                <ul className="footer_links">
                  <li>
                    <a href="#">Popuper Counties</a>
                  </li>
                  <li>
                    <a href="#">Rental Commuities</a>
                  </li>
                  <li>
                    <a href="#">Mortgage</a>
                  </li>
                  <li>
                    <a href="#">Listed Houses</a>
                  </li>
                  <li>
                    <a href="#">Register</a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-xl-3 col-lg-3 col-md-6">
              <div className="footer_widget">
                <div className="footer_title">
                  <h3>About</h3>
                </div>
                <ul className="footer_links">
                  <li>
                    <a href="#">Newsroom</a>
                  </li>
                  <li>
                    <a href="#">Learn about new features</a>
                  </li>
                  <li>
                    <a href="#">Letter from our founders</a>
                  </li>
                  <li>
                    <a href="#">Careers</a>
                  </li>
                  <li>
                    <a href="#">Investors</a>
                  </li>
                  <li>
                    <a href="#">Property Features</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="copyright_area">
        <div className="container-fluid">
          <div className="footer_border"></div>
          <div className="row">
            <div className="col-md-12">
              <div className="copyright_wrapper d-flex flex-wrap gap_20">
                <div className="copy_right_text flex-fill">
                  <p>
                    &copy;2022 RedQ, Inc. <a href="#">Privacy</a>
                    <span className="dot_devide"></span>
                    <a href="#">Terms</a>
                    <span className="dot_devide"></span>
                    <a href="#">Sitemap</a>
                  </p>
                </div>
                <div className="footerLinks_right d-flex flex-wrap">
                  <div className="language_links d-flex flex-wrap">
                    <a className="d-flex align-items-center gap_10" href="#">
                      <div className="icon d-flex align-items-center ">
                        <svg
                          width="16"
                          height="16"
                          viewBox="0 0 16 16"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            d="M15.25 8C15.25 12.0041 12.0041 15.25 8 15.25C3.99594 15.25 0.75 12.0041 0.75 8C0.75 3.99594 3.99594 0.75 8 0.75C12.0041 0.75 15.25 3.99594 15.25 8Z"
                            stroke="#4B5563"
                            strokeWidth="1.5"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                          />
                          <path
                            d="M11.25 8C11.25 12.5 9.24264 15.25 8 15.25C6.75736 15.25 4.75 12.5 4.75 8C4.75 3.5 6.75736 0.75 8 0.75C9.24264 0.75 11.25 3.5 11.25 8Z"
                            stroke="#4B5563"
                            strokeWidth="1.5"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                          />
                          <path
                            d="M1 8H15"
                            stroke="#4B5563"
                            strokeWidth="1.5"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                          />
                        </svg>
                      </div>
                      <span>English (US)</span>
                    </a>
                    <a className="d-flex align-items-center gap_10" href="#">
                      <div className="icon">
                        <svg
                          width="12"
                          height="16"
                          viewBox="0 0 12 16"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            d="M11.25 2.75H3.375C1.92525 2.75 0.75 3.92525 0.75 5.375C0.75 6.82475 1.92525 8 3.375 8H8.625C10.0747 8 11.25 9.17525 11.25 10.625C11.25 12.0747 10.0747 13.25 8.625 13.25H0.75"
                            stroke="#4B5563"
                            strokeWidth="1.5"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                          />
                          <path
                            d="M5.75 15.25V0.75"
                            stroke="#4B5563"
                            strokeWidth="1.5"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                          />
                        </svg>
                      </div>
                      <span>USD</span>
                    </a>
                  </div>
                  <div className="social__Links d-flex flex-wrap align-items-center">
                    <a href="#">
                      <i className="fab fa-facebook"></i>
                    </a>
                    <a href="#">
                      <i className="fab fa-twitter"></i>
                    </a>
                    <a href="#">
                      <i className="fab fa-instagram"></i>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
