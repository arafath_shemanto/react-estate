import Header from "./Layouts/Header";
import Footer from "./Layouts/Footer";
import Home from "./Pages/Home";
import ScrollButton from "./Components/ScrollButton/ScrollButton";

function App() {
  return (
    <>
      <Header />
      <Home />
      <Footer />
      <ScrollButton />
    </>
  );
}

export default App;
