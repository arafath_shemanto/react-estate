import React, { useState } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import { Autoplay, Pagination, Navigation } from "swiper";
import SectionTitle from "../Title/SectionTitle";
import featuresData from "../../Data/Featured.json";
import SingleFeatures from "./SingleFeatures";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
const Featured = () => {
  const [features, setFeatures] = useState(featuresData);
  return (
    <div className="section_spacing">
      <SectionTitle title="Featured Properties" />
      <Swiper
        pagination={{
          type: "progressbar",
        }}
        loop={true}
        breakpoints={{
          640: {
            slidesPerView: 1,
          },
          768: {
            slidesPerView: 2,
          },
          992: {
            slidesPerView: 2,
          },
          1200: {
            slidesPerView: 3,
          },
        }}
        spaceBetween={26}
        modules={[Navigation, Autoplay, Pagination]}
      >
        {features?.map((featureItem) => {
          return (
            <SwiperSlide key={featureItem?.id}>
              <SingleFeatures featureItem={featureItem} />
            </SwiperSlide>
          );
        })}
      </Swiper>
    </div>
  );
};

export default Featured;
