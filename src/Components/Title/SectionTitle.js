import React from "react";

const SectionTitle = ({ title }) => {
  return (
    <div className="section__title mb_24">
      <h3 className="heading_text m-0">{title}</h3>
    </div>
  );
};

export default SectionTitle;
