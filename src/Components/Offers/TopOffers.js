import React, { useState } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import { Autoplay, Pagination, Navigation } from "swiper";
import offersData from "../../Data/Offers.json";
import SingleOffer from "./SingleOffer";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
const TopOffers = () => {
  const [offers, setOffers] = useState(offersData);
  return (
    <div className="section_spacing2">
      <div className="section__title mb_24 d-flex align-items-center gap_10 ">
        <h3 className="heading_text flex-fill m-0">Top Offers</h3>
        <a className="arrow_btn" href="#">
          <span>Show All</span> <i className="fas fa-angle-right"></i>
        </a>
      </div>
      <Swiper
        pagination={{
          type: "progressbar",
        }}
        loop={true}
        breakpoints={{
          580: {
            slidesPerView: 1,
          },
          768: {
            slidesPerView: 2,
          },
          992: {
            slidesPerView: 3,
          },
          1200: {
            slidesPerView: 3,
          },
          1450: {
            slidesPerView: 4,
          },
          1730: {
            slidesPerView: 5,
          },
        }}
        spaceBetween={24}
        modules={[Navigation, Autoplay, Pagination]}
      >
        {offers?.map((offerItem) => {
          return (
            <SwiperSlide key={offerItem.id}>
              <SingleOffer offerItem={offerItem} />
            </SwiperSlide>
          );
        })}
      </Swiper>
    </div>
  );
};

export default TopOffers;
