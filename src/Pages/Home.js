import React from "react";
import Featured from "../Components/Featured/Featured";
import TopOffers from "../Components/Offers/TopOffers";

const Home = () => {
  return (
    <>
      <Featured />
      <TopOffers />
    </>
  );
};

export default Home;
